package com.ymt.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.ymt.test")
@SpringBootApplication
public class SpringRestWithoutReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestWithoutReactiveApplication.class, args);
	}
}
