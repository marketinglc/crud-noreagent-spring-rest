package com.ymt.test.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.ymt.test.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/file")
public class FileController {
    @Autowired(required = true)
    FileUploadService fileUploadSevice;
    @ResponseBody
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Map<String, Object> uploadFile(@RequestParam("file") MultipartFile
                                                  file) {
        Map<String, Object> map = new LinkedHashMap<>();
        try {
            fileUploadSevice.uploadFile(file);
            map.put("result", "file uploaded");
            System.out.println("subio");
        } catch (IOException e) {
            map.put("result", "error while uploading : "+e.getMessage());
            System.out.println("no subio");
        }
        return map;
    }
}
