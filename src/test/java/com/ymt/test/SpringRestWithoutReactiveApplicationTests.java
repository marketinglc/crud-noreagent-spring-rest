package com.ymt.test;

import com.ymt.test.model.User;
import com.ymt.test.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringRestWithoutReactiveApplicationTests {

    @Autowired
    UserService userService;

    @Test
    public void contextLoads() {
        //List<User> users = userService.getAllUsers();
        //assertEquals(3, users.size());

        User user = userService.getUser(101);
        assertTrue(user.getUsername().contains("Bruno"));
    }

}
